let output = document.getElementById("output");

function typeContent(e){
	let content = e.target.innerHTML;
	output.value += content;
};

function clearAll(){
	output.value = "";
	confetti.stop();
}

function backSpace(){
	output.value = output.value.slice(0,-1);
}

function spaceBar(){
	output.value += " ";
}

function easterEggs(){
	let page = document.getElementById("pagebody")
	let wow = document.getElementById("wow")
	if(output.value == "JINO"){
		confetti.start();
	}else if(output.value.slice(0, 3) == "WOW"){
		wow.play()
	}else if(output.value.slice(0, 11) == "BARREL ROLL"){
		page.classList.toggle("animated");
		page.classList.toggle("rotateIn");
		setTimeout(function(){
			page.classList.toggle("animated");
			page.classList.toggle("rotateIn");
		}, 2000); 
	}
}

function enterKey(){
	easterEggs();
	output.value += "\n";
}

let input = document.getElementsByClassName("input");
for(let i = 0; i < input.length; i++){
  input[i].addEventListener("click", typeContent);
}

let clearall = document.getElementById("clearall");
clearall.addEventListener("click", clearAll);

let backspace = document.getElementById("backspace");
backspace.addEventListener("click", backSpace);

let enter = document.getElementById("enter");
enter.addEventListener("click", enterKey);

let space = document.getElementById("space");
space.addEventListener("click", spaceBar);